# 1. 자바 기초

01. [자바 프로그래밍 시작하기](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-01/README.md)
02. [자바와 개발환경 설치하고 첫 프로그래밍 만들기](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-02/README.md)
03. [컴퓨터에서는 자료를 어떻게 표현할까요?](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-03/README.md)
04. [변수는 변하는 수입니다](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-04/README.md)
05. [자료형(data type)- 정수는 어떻게 표현하나요?](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-05/README.md)
06. [자료형(data type)- 실수는 어떻게 표현하여 사용하나요?](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-06/README.md)
07. [자료형(data type)- 문자는 프로그램에서 어떻게 표현하여 사용하나요?](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-07/README.md)
08. [자료형(data type)- 논리형과 자료형 없이 변수 사용하기](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-08/README.md)
09. [변하지 않는 상수와 리터럴, 변수의 형 변환](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-09/README.md)
10. [자바의 연산자들 -1 (대입, 부호, 산술, 복합대입, 증감연산자)](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-10/README.md)
11. [자바의 연산자들 -2 (관계, 논리 연산자)](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-11/README.md)
12. [자바의 연산자들 -3 (조건 연산자, 비트 연산자)](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-12/README.md)
13. [조건문 - if 문(만약에... 라면)](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-13/README.md)
14. [조건이 여러 개 일 때 간단히 표현되는 switch-case문](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-14/README.md)
15. [반복문 - while문, do-while문, for문](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-15/README.md)
16. [반복문이 여러 번 포개진 중첩 반복문](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-16/README.md)
17. [중간에 멈추는 break문, 무시하고 계속 진행하는 continue문](https://gitlab.com/easyspubjava/BeginningJAVA/-/tree/main/Chapter01/01-17/README.md)
